"""
Tests for database reports

"""
from .. import reports
import unittest


class DbReportsTestCase(unittest.TestCase):

    def setUp(self):
        # TODO: put config somewhere sensible.
        self.config = {}
        self.config['dsn'] = 'DRIVER=FreeTDS;' \
                             'SERVER=appdev01.rfspot.com;' \
                             'PORT=1433;' \
                             'DATABASE=db_appdev01;' \
                             'UID=gis_data_editor;' \
                             'PWD=data_editor_0!'
        self.pass_id = 86

    def test_images(self):
        records = reports.images(config=self.config, pass_id=self.pass_id)
        self.assertIsNotNone(records.next())

    def test_barcode_mismatch(self):
        records = reports.barcode_frequency(config=self.config,
                                            pass_id=self.pass_id)
        self.assertIsNotNone(records.next())
