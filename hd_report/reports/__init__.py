"""
reports submodule of hd_report

"""
import pyodbc


def barcode_frequency(config, pass_id):
    """
    ``barcode_frequency`` executes SQL and yields the rows that represent the
    "Barcode Frequency Mismatch" report.

    :param config: Configuration parameter
    :type config: dict
    :param pass_id: Unique pass ID
    :type pass_id: int or str

    """
    sql = """
    SELECT b.id_pass,
           b.barcode,
           b.b_freq,
           qa.barcode,
           qa.qa_freq
    FROM   (SELECT b.id_pass,
                   b.barcode,
                   Count(b.barcode) AS b_freq
            FROM   db_appdev01.gis_data_creator.barcode b
            WHERE  b.id_pass = ?
            GROUP  BY b.id_pass,
                      b.barcode) b
           INNER JOIN (SELECT sub.barcode,
                              sub.time_begin
                       FROM   (SELECT qa.barcode,
                                      qa.time_begin,
                                      Row_number()
                                        OVER (
                                          partition BY b.barcode
                                          ORDER BY Abs(Datediff(mi, qa.time_begin,
                                        b.time_begin))) AS
                                      ranking
                               FROM   db_appdev01.gis_data_creator.barcode b
                                      LEFT OUTER JOIN db_appdev01.gis_data_creator.barcode_qa qa
                                                   ON b.barcode = qa.barcode
                               WHERE  b.id_pass = ?) sub
                       WHERE  sub.ranking = 1) n
                   ON b.barcode = n.barcode
           INNER JOIN (SELECT qa.barcode,
                              qa.time_begin,
                              Count(qa.barcode) AS qa_freq
                       FROM   db_appdev01.gis_data_creator.barcode_qa qa
                       GROUP  BY qa.barcode,
                                 qa.time_begin) qa
                   ON n.time_begin = qa.time_begin
                      AND n.barcode = qa.barcode
    WHERE b.b_freq != qa.qa_freq;"""

    with pyodbc.connect(config['dsn']) as cnxn:
        cnxn.timeout = 15
        with cnxn.cursor() as cursor:
            cursor.execute(sql, (pass_id, pass_id))
            while True:
                row = cursor.fetchone()
                if row:
                    yield row
                else:
                    break


def images(config, pass_id):
    """
    ``images`` executes SQL and yields the rows that represent the "Images"
    report.

    :param config: Configuration parameter
    :type config: dict
    :param pass_id: Unique pass ID
    :type pass_id: int or str

    """
    sql = """
    SELECT t1.id_pass, t1.id_org, id_campus, id_store,
            id_structure, id_floor, id_bay, bay, barcode,
            filename_image, time_begin
    FROM db_appdev01.gis_data_creator.bay_image_lookup t1
    INNER JOIN db_appdev01.gis_data_creator.pass t2
    ON t1.id_pass = t2.id_pass
    WHERE t1.id_pass = ? AND t2.id_pass = ?
    ORDER BY barcode, filename_image"""

    with pyodbc.connect(config['dsn']) as cnxn:
        cnxn.timeout = 5
        with cnxn.cursor() as cursor:
            cursor.execute(sql, (pass_id, pass_id))
            while True:
                row = cursor.fetchone()
                if row:
                    yield row
                else:
                    break
