from gevent import joinall, monkey, spawn
monkey.patch_socket()
from contextlib import closing
from flask import Flask, redirect, render_template, request, url_for, jsonify
from flask.ext.login import current_user, login_required, LoginManager, \
    login_user, UserMixin
from flask.ext.wtf import Form
import json
import reports
from urllib import urlencode
from urllib2 import Request, urlopen
import sqlite3
from time import gmtime, strftime
from wtforms import StringField, PasswordField

# Flask stuff
app = Flask(__name__)
app.jinja_env.globals.update(gmtime=gmtime, strftime=strftime)
app.secret_key = '$2a$12$Zh13lxtcUzqSJUoKgIyKiO'

# Flask-login stuff
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = '/hd_report/login'

# ArcGIS stuff
AGSADMIN_URL = 'http://appdev01.rfspot.com/arcgis/admin'


@login_manager.user_loader
def load_user(userid):
    """
    Flask-login user loader for generating new User objects.

    """
    with sqlite3.connect('db.sqlite3') as conn:
        with closing(conn.cursor()) as cur:
            cur.execute('SELECT password FROM users WHERE name = ?;',
                        (userid,))
            result = cur.fetchone()
            if result:
                return User(username=userid, password=result[0])


def generate_token(agsadmin_url, username, password, expiration=1):
    """
    Generates a new ArcGIS server token for a user.

    :param agsadmin_url: REST admin URL
    :param username: A username
    :param password: A password
    :param expiration: Life of a token in minutes

    """
    endpt = '/'.join((agsadmin_url, 'generateToken'))
    params = {'f': 'json',
              'username': username,
              'password': password,
              'client': 'requestip',
              'expiration': expiration}
    data = urlopen(Request(url=endpt, data=urlencode(params)))
    response = json.loads(data.read())
    token = response.get('token')
    return token


class User(UserMixin):
    """
    Custom User class for use with Flask-login.

    """
    def __init__(self, username, password):
        """
        A User object is initialized with a token that is good for one minute
        by default, which is plenty of time considering that tokens are
        generated frequently as we authenticate requests.

        :param username: A username that exists in ArcGIS server's user store
        :param password: A valid password for that user

        """
        self.username = username
        self.token = generate_token(agsadmin_url=AGSADMIN_URL,
                                    username=username,
                                    password=password)
        if self.is_authenticated():
            # We are storing passwords in plain text for the moment.
            with sqlite3.connect('db.sqlite3') as conn:
                with closing(conn.cursor()) as cur:
                    cur.execute('CREATE TABLE IF NOT EXISTS users ('
                                'name TEXT PRIMARY KEY NOT NULL,'
                                'password TEXT NOT NULL);')
                    cur.execute('INSERT OR IGNORE INTO users'
                                '(name, password) VALUES (?, ?);', (username,
                                                                    password))
                    cur.execute('UPDATE OR IGNORE users SET password = ?'
                                'WHERE name = ?;', (password, username))
                    conn.commit()

    def is_authenticated(self):
        if self.token:
            return True
        return False

    def get_id(self):
        return u'{0}'.format(self.username)


class LoginForm(Form):
    """
    Custom login form using wtforms.

    """
    username = StringField('Username')
    password = PasswordField('Password')


@app.route('/hd_report/login', methods=['GET', 'POST'])
def login():
    """
    Login page.

    """
    error_msg = None
    form = LoginForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,
                    password=form.password.data)
        if user.is_authenticated():
            login_user(user)
            return redirect(request.args.get('next') or url_for('index'))
        error_msg = 'Invalid credentials'
    return render_template('login.html', form=form, error_msg=error_msg)


@app.route('/hd_report/', methods=['GET', 'POST'])
@login_required
def index(id_pass=0):
    """
    Our landing page after login.

    """
    if request.method == 'POST':
        id_pass = request.form['id_pass'] or id_pass
        return redirect(url_for('report', id_pass=id_pass))
    else:
        response = query_task(layer='http://appdev01.rfspot.com/arcgis/rest'
                                    '/services/viewer_3/MapServer/5',
                              params={'where': 'id_org=3 and id_status>0',
                                      'outFields': 'id_pass,time_begin,'
                                                   'time_end',
                                      'returnGeometry': 'false',
                                      'orderByFields': 'id_pass desc',
                                      'returnDistinctValues': 'true'},
                              token=current_user.token)

        passes = response.get('features')[:20]
        return render_template('recent_passes.html', passes=passes)


@app.route('/hd_report/rotate_img/<org>/<campus>/<store>/<structure>/<floor>'
           '/<time>/<id_pass>/<image>')
@login_required
def rotate_img(org, campus, store, structure, floor, time, id_pass, image):
    """
    Rotates the barcode images 270 degrees.

    """
    srcpath = '/image/org_{0}/campus_{1}/store_{2}/structure_{3}/floor_{4}' \
              '/{5}/pass_{6}/{7}'.format(org, campus, store, structure, floor,
                                         time, id_pass, image)
    return render_template('rotate_270.html', srcpath=srcpath)


@app.route('/hd_report/<int:id_pass>', methods=['GET'])
@login_required
def report(id_pass=0):
    # Barcode QA Summary Statistics
    q_summary = ('http://appdev01.rfspot.com/arcgis/rest/services/viewer_3'
                 '/MapServer/19',
                 {'where': 'id_pass={0}'.format(id_pass),
                  'outFields': 'id_pass,id_bay,bay,time_begin,barcode_count,'
                               'barcode_qa_count,barcode_count_pct,barcode,'
                               'shelf,sequence,id_tag_size,id_tag_type,'
                               'in_stock,validate,validated',
                  'returnGeometry': 'false',
                  'orderByFields': 'id_pass,bay,time_begin'},
                 current_user.token)

    # Product Stock Level Less than 100%
    q_stock = ('http://appdev01.rfspot.com/arcgis/rest/services/viewer_3'
               '/MapServer/2',
               {'where': 'id_pass={0} and in_stock < 100 '
                         'and id_tag_type <> 3'.format(id_pass),
                'outFields': 'id_pass,id_bay,bay,shelf,sequence,barcode,'
                             'in_stock,in_stock_confidence',
                'returnGeometry': 'false',
                'orderByFields': 'bay,in_stock,sequence,barcode'},
               current_user.token)

    # Spawn coroutines
    tasks = [spawn(query_task, *q_summary),
             spawn(query_task, *q_stock)]
    joinall(tasks, timeout=60)

    # Direct database queries, TODO: put config somewhere sensible.
    config = {}
    config['dsn'] = 'DRIVER=FreeTDS;' \
                    'SERVER=appdev01.rfspot.com;' \
                    'PORT=1433;' \
                    'DATABASE=db_appdev01;' \
                    'UID=gis_data_editor;' \
                    'PWD=data_editor_0!'

    # TODO: consider using concurrent.futures.ProcessPoolExecutor().
    images = list(reports.images(config=config, pass_id=id_pass))

    return render_template('report.html', id_pass=id_pass,
                           summary_columns=tasks[0].value.get('fields'),
                           stock_columns=tasks[1].value.get('fields'),
                           summary_rows=tasks[0].value.get('features'),
                           stock_rows=tasks[1].value.get('features'),
                           images_rows=images,
                           barcode_mismatch_rows=None)  # TODO: Update template


@app.route('/hd_report/<int:id_pass>/barcode_mismatch', methods=['GET'])
@login_required
def barcode_mismatch_json(id_pass=0):
    """
    Returns a JSON representation of the barcode mismatch report for a given
    pass.

    """
    # Direct database queries, TODO: put config somewhere sensible.
    config = {}
    config['dsn'] = 'DRIVER=FreeTDS;' \
                    'SERVER=appdev01.rfspot.com;' \
                    'PORT=1433;' \
                    'DATABASE=db_appdev01;' \
                    'UID=gis_data_editor;' \
                    'PWD=data_editor_0!'
    barcode_mismatch = reports.barcode_frequency(config=config,
                                                 pass_id=id_pass)

    result = []
    for row in barcode_mismatch:
        columns = [i[0] for i in row.cursor_description]
        result.append(dict(zip(columns, row)))
    return jsonify(results=result)


def query_task(layer=None, params={}, token=None):
    """
    Query a layer and return results.

    :param layer: URL to a layer
    :param params: parameters of the query

    """
    request = 'query'

    params['f'] = 'json'
    if token:
        params['token'] = token

    endpt = '/'.join((layer, request))
    data = urlopen(Request(url=endpt, data=urlencode(params)))
    response = json.loads(data.read())
    return response


if __name__ == '__main__':
    app.run(debug=True)
