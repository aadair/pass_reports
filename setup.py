"""
Setup.py script for hd_report

"""
from setuptools import setup, find_packages

def readme(filepath):
    """
    Returns contents of a readme file as a single string.

    """
    with open(filepath, 'r') as rm:
        return rm.read()


setup_options = dict(name='hd_report',
                     version='0.1',
                     description='RFSpot Home Depot reporting application',
                     license='Other/Proprietary License',
                     long_description=readme('README.rst'),
                     author='Allan Adair',
                     author_email='allan@rfspot.com',
                     url='https://github.com/RFSpot/services',
                     packages=find_packages('.'),
                     package_dir={'hd_report': 'hd_report'},
                     package_data={'': ['*.txt', '*.rst'],
                                   'hd_report': ['static/*',
                                                 'templates/*']},
                     install_requires=['Flask==0.10.1',
                                       'Flask-Login==0.2.11',
                                       'Flask-WTF==0.11',
                                       'gevent==1.0.2',
                                       'greenlet==0.4.7',
                                       'itsdangerous==0.24',
                                       'Jinja2==2.7.3',
                                       'MarkupSafe==0.23',
                                       'uWSGI==2.0.10',
                                       'Werkzeug==0.10.4',
                                       'WTForms==2.0.2'],
                     classifiers=('Development Status :: 1 - Planning',
                                  'Intended Audience :: Developers',
                                  'Intended Audience :: System Administrators',
                                  'Natural Language :: English',
                                  'License :: Other/Proprietary License',
                                  'Programming Language :: Python',
                                  'Programming Language :: Python :: 2.7'))
setup(**setup_options)
